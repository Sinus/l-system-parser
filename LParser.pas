{Author : Marcin Dominiak}
program LParser;

const
    const_numberOfCharacter = 256; {total number of characters - size of array for interpretation etc.}

type
    rules = array[0..const_numberOfCharacter,0..1] of string;
    signs = array[0..const_numberOfCharacter] of boolean;

procedure prepareArrays(var substitutionRules,interpretationRules:rules;var signsToBeReplaced,signsToBeInterpretated:signs);
{filles arrays with false and empty strings}
var 
    i:integer;
begin
    i:=0;
    while i<=const_numberOfCharacter do
    begin
        substitutionRules[i,0]:='';
        substitutionRules[i,1]:='';
        interpretationRules[i,0]:='';
        interpretationRules[i,1]:='';
        signsToBeReplaced[i]:=false;
        signsToBeInterpretated[i]:=false;
        inc(i);
    end;
end;


procedure readData(var substitutionRules:rules;var signsToBeReplaced:signs); 
{reads substitution and interpretation rules}
var 
    readLine,rule:string;
    i:integer;
begin
    i:=0;
    readLine:='rules'; {not empty string is hardcoded so that we will enter the loop}
    while readLine<>'' do
    begin
        readln(readLine);
        substitutionRules[i,0]:=readLine[1];
        signsToBeReplaced[ord(readLine[1])]:=true;
        rule:=Copy(readLine,2,length(readLine)-1); {sign that will be changed is the 1st, rest is copied to the substitution array}
        substitutionRules[i,1]:=rule;
        inc(i);
    end;
end;

function getReplacement(const character:char;const setOfRules:rules):string;
{returns substitution/interpretation for given character from given array setOfRules}
var
    i:integer;
begin
    i:=0;
    getReplacement:='';
    while (i<const_numberOfCharacter) and (getReplacement='') do
    begin 
        if setOfRules[i,0]=character then
            getReplacement:=setOfRules[i,1];
        inc(i);
    end;
end;

procedure getEpilogue();
{reads and writes epilogue/prologue}
var
    readLine:ansistring;
begin
    readLine:='logue'; {not empty string is hardcoded so we will enter the loop}
    while readLine<>'' do
    begin
        readln(readLine);
        if readLine<>'' then
            writeln(readLine);
    end;
end;

var
    treeDepth,iteration:integer;
    i:longint;
    substitutionRules,interpretationRules:rules;
    currentWord,nextWord:ansistring;
    signsToBeReplaced,signsToBeInterpretated:signs;
begin
    iteration:=0;
    nextWord:='';
    prepareArrays(substitutionRules,interpretationRules,signsToBeReplaced,signsToBeInterpretated);

    readln(treeDepth);
    readln(currentWord);
    readData(substitutionRules,signsToBeReplaced);
    getEpilogue(); {prologue}
    readData(interpretationRules,signsToBeInterpretated); 

    while iteration<treeDepth do
    {substitution stage}
    begin
        i:=1;
        while i<=length(currentWord) do
        begin
            if signsToBeReplaced[ord(currentWord[i])]=true then
                nextWord:=nextWord+getReplacement(currentWord[i],substitutionRules)
            else
                nextWord:=nextWord+currentWord[i];
            inc(i);
        end;
        inc(iteration);
        currentWord:=nextWord;
        nextWord:='';
    end;
    
    i:=1;
    while i<=length(currentWord) do
    {interpretation stage}
    begin
        if signsToBeInterpretated[ord(currentWord[i])]=true then
            writeln(getReplacement(currentWord[i],interpretationRules)); {signs that are not interpreted are not displayed}
        inc(i);
    end;

    getEpilogue();
end.
