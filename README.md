LParser is a program that uses Lindenmayer's system (L-system).
What it is and how it works can be found here: http://en.wikipedia.org/wiki/L-system

Input:
1st line determines how many times we iterate over the axiom, how many times is it modified.
2nd line is the axiom.
Until an empty line input contains rules of changing axiom.
1st letter of the lines is the letter that will be chagned, while the rest is what will it be changed to, for example "110" means, that each appearance of "1" in an axiom will be changed to "10".

After an empty line prologue is read.
It is not proccesed, just written out.
It may not contain an empty line.
It finishes with an empty line.

After an empty line interpretation rules are read.
It works just like in the 1st stage, but is not used untill iterating over axiom is finished.
After the axiom is changed as many times as we want (which is specified by 1st line of input) the word is interpreted as specified by interpretation rules.
Example interpretation rule: "1go".
It means that every appearance of "1" in the final word will be changed to "go".

After an empty line epilogue is read.
It works just like prologue.
After an empty line, the program finishes it's work.

Output consists of:
-unchanged prologue

-interpreted axiom

-unchanged epilogue

The program passed all tests (scoring 10/10 for correctness).
It received 10/10 points for style, getting a total of 20/20 points.
